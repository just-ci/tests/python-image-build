# [1.5.0](https://gitlab.com/just-ci/tests/python-image-build/compare/v1.4.0...v1.5.0) (2022-08-04)


### Features

* use beta ([9952701](https://gitlab.com/just-ci/tests/python-image-build/commit/99527010d3782d3ace1b6a0768cdb4f896640e01))

# [1.4.0](https://gitlab.com/just-ci/tests/python-image-build/compare/v1.3.2...v1.4.0) (2022-08-04)


### Features

* container image ([8e4e97d](https://gitlab.com/just-ci/tests/python-image-build/commit/8e4e97d20f8c5000ee48d18a39bb4ff478196f39))

## [1.3.2](https://gitlab.com/just-ci/tests/python/compare/v1.3.1...v1.3.2) (2022-08-03)


### Bug Fixes

* try this ([761660e](https://gitlab.com/just-ci/tests/python/commit/761660e686224c9915d63edd0da3859bf6db7248))
