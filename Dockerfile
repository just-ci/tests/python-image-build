ARG PYVERSION=3.10-slim
FROM python:$PYVERSION

COPY . /app

WORKDIR /app

RUN pip install .
